A work in progress engine by [Linus Sköld](http://linusskold.com).

This engine is being developed as a learning tool, to get a deeper understanding of how things work.