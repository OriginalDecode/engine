#pragma once
#include "../../standard_datatype.hpp"
constexpr u64 EngineEvents_OnActive = 0x33ad8d01b888aea;
constexpr u64 EngineEvents_OnMinimize = 0x33ad8d082817a90;
constexpr u64 EngineEvents_OnMaximize = 0x33ad8d0fae0379a;
constexpr u64 EngineEvents_OnInactive = 0x33ad8d0b40dedfd;
