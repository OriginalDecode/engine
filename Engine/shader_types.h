#pragma once

#ifndef SHADER
#include <Math/Vector/Vector.h>
#include <Math/Matrix/Matrix.h>

typedef CU::Vector4f float4;
typedef CU::Vector3f float3;
typedef CU::Vector2f float2;

typedef CU::Matrix44f float4x4;
typedef CU::Matrix33f float3x3;
#endif
